Will now open ./matrices//ldoor/ldoor.mtx
           0/23737339 :   0% [>                    ]      237373/23737339 :   1% [>                    ]      474746/23737339 :   2% [>                    ]      712120/23737339 :   3% [>                    ]      949493/23737339 :   4% [>                    ]     1186866/23737339 :   5% [>                   ]     1424240/23737339 :   6% [>                   ]     1661613/23737339 :   7% [>                   ]     1898987/23737339 :   8% [>                   ]     2136360/23737339 :   9% [>                   ]     2373733/23737339 :  10% [=>                  ]     2611107/23737339 :  11% [=>                  ]     2848480/23737339 :  12% [=>                  ]     3085854/23737339 :  13% [=>                  ]     3323227/23737339 :  14% [=>                  ]     3560600/23737339 :  15% [==>                 ]     3797974/23737339 :  16% [==>                 ]     4035347/23737339 :  17% [==>                 ]     4272721/23737339 :  18% [==>                 ]     4510094/23737339 :  19% [==>                 ]     4747467/23737339 :  20% [===>                ]     4984841/23737339 :  21% [===>                ]     5222214/23737339 :  22% [===>                ]     5459587/23737339 :  23% [===>                ]     5696961/23737339 :  24% [===>                ]     5934334/23737339 :  25% [====>               ]     6171708/23737339 :  26% [====>               ]     6409081/23737339 :  27% [====>               ]     6646454/23737339 :  28% [====>               ]     6883828/23737339 :  29% [====>               ]     7121201/23737339 :  30% [=====>              ]     7358575/23737339 :  31% [=====>              ]     7595948/23737339 :  32% [=====>              ]     7833321/23737339 :  33% [=====>              ]     8070695/23737339 :  34% [=====>              ]     8308068/23737339 :  35% [======>             ]     8545442/23737339 :  36% [======>             ]     8782815/23737339 :  37% [======>             ]     9020188/23737339 :  38% [======>             ]     9257562/23737339 :  39% [======>             ]     9494935/23737339 :  40% [=======>            ]     9732308/23737339 :  41% [=======>            ]     9969682/23737339 :  42% [=======>            ]    10207055/23737339 :  43% [=======>            ]    10444429/23737339 :  44% [=======>            ]    10681802/23737339 :  45% [========>           ]    10919175/23737339 :  46% [========>           ]    11156549/23737339 :  47% [========>           ]    11393922/23737339 :  48% [========>           ]    11631296/23737339 :  49% [========>           ]    11868669/23737339 :  50% [=========>          ]    12106042/23737339 :  51% [=========>          ]    12343416/23737339 :  52% [=========>          ]    12580789/23737339 :  53% [=========>          ]    12818163/23737339 :  54% [=========>          ]    13055536/23737339 :  55% [==========>         ]    13292909/23737339 :  56% [==========>         ]    13530283/23737339 :  57% [==========>         ]    13767656/23737339 :  58% [==========>         ]    14005030/23737339 :  59% [==========>         ]    14242403/23737339 :  60% [===========>        ]    14479776/23737339 :  61% [===========>        ]    14717150/23737339 :  62% [===========>        ]    14954523/23737339 :  63% [===========>        ]    15191896/23737339 :  64% [===========>        ]    15429270/23737339 :  65% [============>       ]    15666643/23737339 :  66% [============>       ]    15904017/23737339 :  67% [============>       ]    16141390/23737339 :  68% [============>       ]    16378763/23737339 :  69% [============>       ]    16616137/23737339 :  70% [=============>      ]    16853510/23737339 :  71% [=============>      ]    17090884/23737339 :  72% [=============>      ]    17328257/23737339 :  73% [=============>      ]    17565630/23737339 :  74% [=============>      ]    17803004/23737339 :  75% [==============>     ]    18040377/23737339 :  76% [==============>     ]    18277751/23737339 :  77% [==============>     ]    18515124/23737339 :  78% [==============>     ]    18752497/23737339 :  79% [==============>     ]    18989871/23737339 :  80% [===============>    ]    19227244/23737339 :  81% [===============>    ]    19464617/23737339 :  82% [===============>    ]    19701991/23737339 :  83% [===============>    ]    19939364/23737339 :  84% [===============>    ]    20176738/23737339 :  85% [================>   ]    20414111/23737339 :  86% [================>   ]    20651484/23737339 :  87% [================>   ]    20888858/23737339 :  88% [================>   ]    21126231/23737339 :  89% [================>   ]    21363605/23737339 :  90% [=================>  ]    21600978/23737339 :  91% [=================>  ]    21838351/23737339 :  92% [=================>  ]    22075725/23737339 :  93% [=================>  ]    22313098/23737339 :  94% [=================>  ]    22550472/23737339 :  95% [==================> ]    22787845/23737339 :  96% [==================> ]    23025218/23737339 :  97% [==================> ]    23262592/23737339 :  98% [==================> ]    23499965/23737339 :  99% [==================> ]    23737338/23737339 : 100% [===================>]
-> number of rows = 952203
-> number of columns = 952203
-> number of values = 46522475
-> number of values per row = 48.8577
-> number loops to smooth the timing if > 1 =  16
-> number of flops to do per spmv = 1488719200
Start usual CSR: 952203
Conversion in : 0.529155s
-> Done in 4.1241s
-> GFlops 0.36098s
-> Esimated performance are 6.8861 for 2rVc
Start usual 1rVc: 
Conversion in : 6.28952s
-> Done in 1.10924s
-> Number of blocks 5195701( avg. 8.95403 values per block)
-> GFlops 1.34211s
-> Max Difference in Accuracy 48
Start usual 1rVc_v2: 
Conversion in : 6.28469s
-> Done in 1.30685s
-> Number of blocks 5195701( avg. 8.95403 values per block)
-> GFlops 1.13916s
-> Max Difference in Accuracy 48
Start usual 2rVc: 
Conversion in : 6.56896s
-> Done in 1.21862s
-> Number of blocks 2832389( avg. 16.4252 values per block)
-> GFlops 1.22165s
-> Max Difference in Accuracy 48
Start usual 2rVc_v2: 
Conversion in : 6.56825s
-> Done in 1.56076s
-> Number of blocks 2832389( avg. 16.4252 values per block)
-> GFlops 0.953845s
-> Max Difference in Accuracy 48
Start usual 4rVc: 
Conversion in : 6.56972s
-> Done in 1.50294s
-> Number of blocks 1650473( avg. 28.1874 values per block)
-> GFlops 0.990539s
-> Max Difference in Accuracy 48
Start usual 4rVc_v2: 
Conversion in : 6.56895s
-> Done in 1.72813s
-> Number of blocks 1650473( avg. 28.1874 values per block)
-> GFlops 0.861464s
-> Max Difference in Accuracy 48
