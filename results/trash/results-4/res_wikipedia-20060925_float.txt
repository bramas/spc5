-> number of rows = 2048
-> number of columns = 2048
-> number of values = 4194304
-> number of values per row = 2048
-> number loops to smooth the timing if > 1 =  16
-> number of flops to do per spmv = 134217728
Start usual CSR: 2048
Conversion in : 0.0507227s
-> Done in 0.339177s
-> GFlops 0.395716s
-> Esimated performance are 7.16894 for 4rV2c
Start usual 1rVc: 
Conversion in : 0.816532s
-> Done in 0.0241929s
-> Number of blocks 262144( avg. 16 values per block)
-> GFlops 5.54783s
-> Max Difference in Accuracy 0
Start usual 1rVc_v2: 
Conversion in : 0.814413s
-> Done in 0.0274319s
-> Number of blocks 262144( avg. 16 values per block)
-> GFlops 4.89276s
-> Max Difference in Accuracy 0
Start usual 2rVc: 
Conversion in : 0.842728s
-> Done in 0.0200049s
-> Number of blocks 131072( avg. 32 values per block)
-> GFlops 6.70926s
-> Max Difference in Accuracy 0
Start usual 2rVc_v2: 
Conversion in : 0.842613s
-> Done in 0.0271574s
-> Number of blocks 131072( avg. 32 values per block)
-> GFlops 4.94222s
-> Max Difference in Accuracy 0
Start usual 4rVc: 
Conversion in : 0.837035s
-> Done in 0.0217388s
-> Number of blocks 65536( avg. 64 values per block)
-> GFlops 6.17412s
-> Max Difference in Accuracy 0
Start usual 4rVc_v2: 
Conversion in : 0.833265s
-> Done in 0.0238765s
-> Number of blocks 65536( avg. 64 values per block)
-> GFlops 5.62132s
-> Max Difference in Accuracy 0
