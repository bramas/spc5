-> number of rows = 2048
-> number of columns = 2048
-> number of values = 4194304
-> number of values per row = 2048
-> number loops to smooth the timing if > 1 =  16
-> number of flops to do per spmv = 134217728
Start usual CSR: 2048
Conversion in : 0.0503651s
-> Done in 0.338991s
-> GFlops 0.395933s
-> Esimated performance are 7.16894 for 4rV2c
Start usual 1rVc: 
Conversion in : 0.818286s
-> Done in 0.0241749s
-> Number of blocks 262144( avg. 16 values per block)
-> GFlops 5.55194s
-> Max Difference in Accuracy 0
Start usual 1rVc_v2: 
Conversion in : 0.810932s
-> Done in 0.0274273s
-> Number of blocks 262144( avg. 16 values per block)
-> GFlops 4.89357s
-> Max Difference in Accuracy 0
Start usual 2rVc: 
Conversion in : 0.840623s
-> Done in 0.0200086s
-> Number of blocks 131072( avg. 32 values per block)
-> GFlops 6.70799s
-> Max Difference in Accuracy 0
Start usual 2rVc_v2: 
Conversion in : 0.84319s
-> Done in 0.0271603s
-> Number of blocks 131072( avg. 32 values per block)
-> GFlops 4.9417s
-> Max Difference in Accuracy 0
Start usual 4rVc: 
Conversion in : 0.834487s
-> Done in 0.0217403s
-> Number of blocks 65536( avg. 64 values per block)
-> GFlops 6.17369s
-> Max Difference in Accuracy 0
Start usual 4rVc_v2: 
Conversion in : 0.834303s
-> Done in 0.0238642s
-> Number of blocks 65536( avg. 64 values per block)
-> GFlops 5.62423s
-> Max Difference in Accuracy 0
