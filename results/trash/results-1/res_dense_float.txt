-> number of rows = 8000
-> number of columns = 8000
-> number of values = 64000000
-> number of values per row = 8000
-> number loops to smooth the timing if > 1 =  16
-> number of flops to do per spmv = 2048000000
Start usual CSR: 8000
Conversion in : 0.792993s
-> Done in 5.21454s
-> GFlops 0.392748s
-> Esimated performance are 3.62819 for 2rV2c_WT
Start usual 1rVc: 
Conversion in : 7.17506s
-> Done in 2.54368s
-> Number of blocks 8000000( avg. 8 values per block)
-> GFlops 0.805132s
-> Max Difference in Accuracy 0
Start usual 2rVc: 
Conversion in : 6.95244s
-> Done in 0.612147s
-> Number of blocks 4000000( avg. 16 values per block)
-> GFlops 3.3456s
-> Max Difference in Accuracy 0
Start usual 4rVc: 
Conversion in : 6.85437s
-> Done in 0.65765s
-> Number of blocks 2000000( avg. 32 values per block)
-> GFlops 3.11412s
-> Max Difference in Accuracy 0
===================================================
Openmp is enabled with 48 threads per default
Estimate for omp: 8000
-> Esimated threaded performance are 71.582 for 4rVc with 48
Start openmp 1rVc: 
-> Done in 2.56811s
-> Number of blocks 8000000( avg. 8 values per block)
-> GFlops 0.797473s
-> Max Difference in Accuracy 0
Start openmp 2rVc: 
-> Done in 0.639047s
-> Number of blocks 4000000( avg. 16 values per block)
-> GFlops 3.20477s
-> Max Difference in Accuracy 0
Start openmp 4rVc: 
-> Done in 0.670366s
-> Number of blocks 2000000( avg. 32 values per block)
-> GFlops 3.05505s
-> Max Difference in Accuracy 0
